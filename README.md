# WeGo

[![pipeline status](https://gitlab.com/Iadgarov/wego/badges/master/pipeline.svg)](https://gitlab.com/Iadgarov/wego/commits/master) [![coverage report](https://gitlab.com/Iadgarov/wego/badges/master/coverage.svg)](https://gitlab.com/Iadgarov/wego/commits/master) [![Go Report Card](https://goreportcard.com/badge/gitlab.com/iadgarov/wego)](https://goreportcard.com/report/gitlab.com/iadgarov/wego) [![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0) 

---

This repository is a Go project template providing:
* Gitlab CI
  - Runs Go format and lint
  - Runs unit tests (note that `go vet` is run as part of `go test`)
  - Runs unit tests in race detection configuration
  - Runs unit tests in memory sanitation configuration
  - Checks unit test coverage and generates an html report as a CI artifact
 
* Small and secure Docker image
  - Image is `FROM scratch` - allowing for the minimal needed capabilities and a tiny footprint
  - Use a new, unprivilaged, user in the image
  - Pull the image used in the build phase by digest, not by name. 
  - Explicitly specify th exposed port which is > 1024
  - Install SSL ca certificates
  - Add zoneinfo for timezones

* Go project structure based on convention - `https://github.com/golang-standards/project-layout`


## CI Variables

| Variable | Description | Default Value |
| ------ | ------ | ------ |
| `MIN_COVERAGE` | Lowest acceptable code coverage precentage (as integer). | `80` |
| `COVERAGE_REPORT_ENABLED` | Boolean indicating whether a code coverage html report should be generated.<br />These reports are saved as CI artifacts. | `true` | 


## Gitlab CI for Go

Inspired by this post: `https://medium.com/pantomath/go-tools-gitlab-how-to-do-continuous-integration-like-a-boss-941a3a9ad0b6`

## Small and secure Docker image

Inspired by this post: `https://medium.com/@chemidy/create-the-smallest-and-secured-golang-docker-image-based-on-scratch-4752223b7324`